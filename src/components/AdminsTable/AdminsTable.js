/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import DeleteFilled from '@ant-design/icons/lib/icons/DeleteFilled';
import Spin from 'antd/lib/spin';
import Table from 'antd/lib/table';
import Space from 'antd/lib/space';
import Button from 'antd/lib/button';
import Typography from 'antd/lib/typography';
import deleteAdmin from '../../redux/actions/admin/deleteAdmin';
import getAmdins from '../../redux/actions/admin/getAdmins';
import './index.css';

const { Column } = Table;
const { Title } = Typography;

function AdminsTable({ admins, isLoading, deleteAdmin, users, getAmdins }) {
  useEffect(() => {
    getAmdins();
  }, [users?.deleteAdmin?.message?.status]);

  return (
    <>
      <Table
        className="trip_admins"
        scroll={{ x: true }}
        dataSource={admins}
        loading={users.deleteAdmin.loading || isLoading}
        key={admins.id}
        showHeader={false}
      >
        <Column key="id" render={(text, record) => <div className="trip_admin_profile"></div>} />
        <Column
          className="title_3 names"
          key="id"
          render={(text, record) => (
            <Title level={5}>{`${record.firstName} ${record.lastName}`}</Title>
          )}
        />
        <Column className="text_label bold" dataIndex="email" key="id" />
        <Column
          key="id"
          render={(text, record) => (
            <Space size="middle">
              <Button
                style={{ position: 'relative' }}
                type="primary"
                className="delete_button"
                shape="circle"
                icon={<DeleteFilled />}
                style={{ background: 'orangered', border: 'none' }}
                onClick={() => deleteAdmin(record.id)}
              />
            </Space>
          )}
        />
      </Table>
    </>
  );
}

AdminsTable.propTypes = {
  admins: PropTypes.array,
  isLoading: PropTypes.bool,
  deleteAdmin: PropTypes.func,
  users: PropTypes.object,
  getAmdins: PropTypes.func
};

const mapState = (state) => ({ users: state.admins });
export default connect(mapState, { deleteAdmin, getAmdins })(AdminsTable);
