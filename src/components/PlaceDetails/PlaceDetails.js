import { Fragment } from 'react';
import Layout from 'antd/lib/layout';
import Breadcrumb from 'antd/lib/breadcrumb';
import './index.css';
import RightOutlined from '@ant-design/icons/RightOutlined';
import { Link } from 'react-router-dom';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import Typography from 'antd/lib/typography';
import SingleCard from '../SingleCard';
import TripView from '../TripView';
import Picture from '../../assets/img/gollira.jpg';
import Picture1 from '../../assets/img/kibuye.jpg';
import * as urlHelper from '../../helpers/urlHelper';

const { Content } = Layout;
const { Title } = Typography;

const PlaceDetails = () => {
  const location = window.location.href.split('/');
  const placeId = location[location.length - 1];

  const button = (
    <button className="request_button">
      Request trip <RightOutlined />
    </button>
  );

  return (
    <Fragment>
      <Row style={{ padding: '10px' }}>
        <Col span={16} offset={4}>
          <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>
              <Link to={`/${urlHelper.traveller.index}`}>Explore</Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>Requests</Breadcrumb.Item>
          </Breadcrumb>

          <Content>
            <TripView
              img={places[placeId].img}
              title={places[placeId].name}
              price={places[placeId].price}
              desc={places[placeId].desc}
              footer={button}
            />

            <Title level={3} style={{ padding: '15px 0' }}>
              More Places
            </Title>

            <Row>
              {places.map((place) => (
                <Col lg={8} md={12} sm={24} style={{ marginBottom: '20px' }}>
                  <SingleCard
                    name={place.name}
                    price={place.price}
                    picture={place.img}
                    id={place.id}
                  />
                </Col>
              ))}
            </Row>
          </Content>
        </Col>
      </Row>
    </Fragment>
  );
};

export default PlaceDetails;

const places = [
  {
    id: 0,
    img: Picture,
    name: 'Misisipi',
    price: 3000,
    desc:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis, ex at porttitor dictum, tortor felis aliquet eros, et scelerisque leo arcu a metus. Nam dolor lorem, rutrum vitae enim vitae, luctus ultrices dui. Donec at erat semper, viverra urna sed, molestie velit.'
  },
  {
    id: 1,
    img: Picture1,
    name: 'Kivu lake',
    price: 34534534,
    desc:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis, ex at porttitor dictum, tortor felis aliquet eros, et scelerisque leo arcu a metus. Nam dolor lorem, rutrum vitae enim vitae, luctus ultrices dui. Donec at erat semper, viverra urna sed, molestie velit.'
  },
  {
    id: 2,
    img: Picture1,
    name: 'Kivu lake',
    price: 34534534,
    desc:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis, ex at porttitor dictum, tortor felis aliquet eros, et scelerisque leo arcu a metus. Nam dolor lorem, rutrum vitae enim vitae, luctus ultrices dui. Donec at erat semper, viverra urna sed, molestie velit.'
  },
  {
    id: 3,
    img: Picture1,
    name: 'Kivu lake',
    price: 34534534
  },
  {
    id: 4,
    img: Picture1,
    name: 'Kivu lake',
    price: 34534534,
    desc:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis, ex at porttitor dictum, tortor felis aliquet eros, et scelerisque leo arcu a metus. Nam dolor lorem, rutrum vitae enim vitae, luctus ultrices dui. Donec at erat semper, viverra urna sed, molestie velit.'
  },
  {
    id: 5,
    img: Picture1,
    name: 'Kivu lake',
    price: 34534534,
    desc:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis, ex at porttitor dictum, tortor felis aliquet eros, et scelerisque leo arcu a metus. Nam dolor lorem, rutrum vitae enim vitae, luctus ultrices dui. Donec at erat semper, viverra urna sed, molestie velit.'
  }
];
