import React, { Fragment } from 'react';
import './index.css';
import Image from 'antd/lib/image';
import Layout from 'antd/lib/layout';
import Col from 'antd/lib/col';
import Row from 'antd/lib/row';
import Typography from 'antd/lib/typography';
import ProfilePicture from '../../assets/img/gollira.jpg';
import Profile from '../Profile';
import Logo from '../../assets/img/Group 5.png';

const { Header } = Layout;
const { Title } = Typography;
const Navbar = () => (
  <Fragment>
    <Header className="nav_header">
      <Row wrap={false} align="middle" justify="space-between">
        <Col
          flex="none"
          style={{ display: 'flex', alignItems: 'center', gap: '10px', marginTop: '10px' }}
        >
          <Image preview={false} width={40} src={Logo} />
          <Title className="sm_hide" level={5}>
            WanderLust
          </Title>
        </Col>
        <Profile ProfilePicture={ProfilePicture} />
      </Row>
    </Header>
  </Fragment>
);

export default Navbar;
