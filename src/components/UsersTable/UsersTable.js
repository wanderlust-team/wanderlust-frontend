/* eslint-disable no-unused-vars */
import React from 'react';
import PropTypes from 'prop-types';
import './index.css';
import StarFilled from '@ant-design/icons/StarFilled';
import { connect } from 'react-redux';
import Table from 'antd/lib/table';
import Typography from 'antd/lib/typography';
import Button from 'antd/lib/button';
import updateUser from '../../redux/actions/users/updateUsers';

const { Column } = Table;
const { Title } = Typography;

function UsersTable({ allUsers, isLoading, updateUser }) {
  return (
    <>
      <Table
        scroll={{ x: true }}
        className="trip_admins"
        dataSource={allUsers}
        loading={isLoading}
        key="id"
        showHeader={false}
      >
        <Column key="id" render={(text, record) => <div className="trip_admin_profile"></div>} />
        <Column
          className="title_3"
          key="id"
          render={(text, record) => (
            <Title level={5}>{`${record.firstName} ${record.lastName}`}</Title>
          )}
        />
        <Column className="text_label bold" dataIndex="email" key="id" />
        <Column
          key="id"
          render={(text, record) => (
            <Button
              type="primary"
              className={record.role === 'traveller' ? 'star_button' : 'star_button checked'}
              shape="circle"
              icon={<StarFilled />}
              onClick={() => (record.role === 'traveller' ? updateUser(record.email) : null)}
            />
          )}
        />
      </Table>
    </>
  );
}

UsersTable.propTypes = {
  allUsers: PropTypes.array,
  isLoading: PropTypes.bool,
  updateUser: PropTypes.func
};

const mapState = (state) => ({ users: state.users });
export default connect(mapState, { updateUser })(UsersTable);
