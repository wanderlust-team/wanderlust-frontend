/* eslint-disable camelcase */
import React from 'react';
import PropTypes from 'prop-types';
import './index.css';
import CheckOutlined from '@ant-design/icons/CheckOutlined';

const SingleRequest = ({ name, picture, price, user_email, user_name, status }) => (
  <>
    <div className={status === 'APPROVED' ? 'single_trip_request checked' : 'single_trip_request'}>
      <div className="request_img">
        <img src={picture} alt="request profile" />
      </div>
      <div className="trip_names">
        <p className="text_label bold">{name}</p>
        <p className="title_3 small">{price?.toLocaleString()} Rwf</p>
      </div>
      <div className="trip_names">
        <p className="title_3 small">{user_name}</p>
        <p className="text_label bold">{user_email}</p>
      </div>
      <div className="request_options">
        <button className="delete_button">X</button>
        <button className="tick_button">
          <CheckOutlined />
        </button>
      </div>
    </div>
  </>
);

SingleRequest.propTypes = {
  name: PropTypes.string,
  picture: PropTypes.string,
  price: PropTypes.number,
  user_email: PropTypes.string,
  user_name: PropTypes.string,
  status: PropTypes.string
};

export default SingleRequest;
