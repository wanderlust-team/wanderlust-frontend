import React, { useEffect, useState } from 'react';
import './index.css';
import { Link, useHistory } from 'react-router-dom';
import Menu from 'antd/lib/menu';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import * as urlHelper from '../../helpers/urlHelper';

function SuperAdminNavbar() {
  const [active, setActive] = useState(null);

  const location = useHistory().location.pathname;

  useEffect(() => {
    if (location === '/super-admin') setActive(true);
    else {
      setActive(false);
    }
  }, [location]);

  return (
    <>
      <Row className="super_admin_nav">
        <Col xs={{ span: 24, offset: 0 }} sm={{ span: 16, offset: 4 }}>
          <Menu className="super_admin_nav" mode="horizontal">
            <Menu.Item className={`menu_item ${active ? 'active' : ''}`}>
              <Link to={`/${urlHelper.superAdmin.index}`}>trip admins</Link>
            </Menu.Item>
            <Menu.Item className={`menu_item ${!active ? 'active' : ''}`}>
              <Link to={`/${urlHelper.superAdmin.users}`}>Users</Link>
            </Menu.Item>
          </Menu>
        </Col>
      </Row>
    </>
  );
}

export default SuperAdminNavbar;
