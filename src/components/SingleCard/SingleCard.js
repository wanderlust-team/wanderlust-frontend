import React from 'react';
import PropTypes from 'prop-types';
import RightOutlined from '@ant-design/icons/RightOutlined';
import { Link } from 'react-router-dom';
import * as urlHelper from '../../helpers/urlHelper';
import './index.css';

const SingleCard = ({ status, name, price, id, picture }) => (
  <div className="trip_card">
    <div className="card_cover">
      <img src={picture} alt="card holder" />
    </div>
    <div className="card_desc">
      <div style={{ width: '100%' }}>
        <p className="title_4 card_title text_label">{name}</p>
        <p className="title_2" style={{ marginBottom: '15px' }}>
          {price?.toLocaleString()} Rwf
        </p>
      </div>
      <p className={`bold ${status}`}>{status || null}</p>
      <Link to={`/${urlHelper.traveller.places}/${id}`}>
        <button className="card_button">
          <RightOutlined />
        </button>
      </Link>
    </div>
  </div>
);

SingleCard.propTypes = {
  status: PropTypes.string,
  name: PropTypes.string,
  price: PropTypes.number,
  id: PropTypes.number,
  picture: PropTypes.string
};

export default SingleCard;
