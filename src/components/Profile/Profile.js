import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import './index.css';
import CaretDownOutlined from '@ant-design/icons/CaretDownOutlined';
import Popover from 'antd/lib/popover';
import Col from 'antd/lib/col';
import Image from 'antd/lib/image';
import Typography from 'antd/lib/typography';
import LogoutOutlined from '@ant-design/icons/LogoutOutlined';
import Logout from '../../utils/Logout';
import { userData } from '../../helpers/currentUser';

const { Title } = Typography;

const Profile = ({ ProfilePicture }) => {
  const user = userData();

  const [isVisible, setIsVisible] = useState(false);
  const path = useHistory();

  const content = (
    <div>
      <Title
        level={5}
        className="pointer lg_hide"
        style={{
          fontSize: '12px',
          minWidth: '80px',
          whiteSpace: 'nowrap',
          textOverflow: 'ellipsis',
          overflow: 'hidden'
        }}
      >
        <span className="">{`${user?.user?.user?.firstName} ${user?.user?.user?.lastName} `}</span>
        {user?.user?.user?.role !== 'traveller' ? (
          <Title style={{ lineHeight: '11px', fontSize: '10px' }} level={5}>
            {user?.user?.user.role === 'travelAdmin' ? 'Travel admin' : 'Super admin'}
          </Title>
        ) : (
          ''
        )}
      </Title>

      <LogoutOutlined
        className="logout_button color_orange"
        onClick={() => {
          Logout();
          path.replace('/');
        }}
      />
    </div>
  );

  return (
    <Col
      flex="0 1 1"
      style={{
        display: 'flex',
        alignItems: 'center',
        gap: '10px',
        marginTop: '10px'
      }}
    >
      <Image
        preview={false}
        width={40}
        height={40}
        style={{ borderRadius: '50%' }}
        src={ProfilePicture}
      />

      <Popover
        content={content}
        visible={isVisible}
        trigger="click"
        onVisibleChange={() => setIsVisible(!isVisible)}
      >
        <Title level={5} className="pointer" style={{ fontSize: '14px' }}>
          <span className="sm_hide">
            {`${user?.user?.user?.firstName} ${user?.user?.user?.lastName} `}
          </span>
          <CaretDownOutlined className="drop_down" />
          {user?.user?.user?.role !== 'traveller' ? (
            <Title style={{ lineHeight: '11px', fontSize: '11px' }} className="sm_hide" level={5}>
              {user?.user?.user.role === 'travelAdmin' ? 'Travel admin' : 'Super admin'}
            </Title>
          ) : (
            ''
          )}
        </Title>
      </Popover>
    </Col>
  );
};

Profile.propTypes = { ProfilePicture: PropTypes.string };

export default Profile;
