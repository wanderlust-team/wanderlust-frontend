import axiosHelper from '../helpers/axiosHelper';
import { actionTypes } from '../redux/actionTypes';

const apiMiddleware = ({ dispatch, getState }) => (next) => async ({ type = '', payload = {} }) => {
  if (type !== actionTypes.api.API_REQUEST) {
    return next({ type, payload });
  }

  // CHECK IF TOKEN EXPIRED

  try {
    dispatch({ type: payload.onStart, payload: { loading: true } });
    const { method, url } = payload;
    const { data } = await axiosHelper({})[method](url, payload.data);
    dispatch({ type: payload.onSuccess, payload: data });
  } catch (error) {
    // ROUTING
    const err = error.response ? error.response.data.message : error.message;
    dispatch({
      type: payload.onFailure,
      payload: err
    });
  }
  dispatch({ type: payload.onEnd, payload: { loading: false } });
  return getState();
};

export default apiMiddleware;
