import React, { Fragment } from 'react';
import { Switch, Route, useHistory } from 'react-router-dom';
import * as urlHelper from '../../helpers/urlHelper';
import { userData } from '../../helpers/currentUser';

const Requests = React.lazy(() => import('../../views/Requests'));

// eslint-disable-next-line consistent-return
const travelAdminRoutes = () => {
  const isAuth = userData()?.isAuth;
  const role = userData()?.user?.user.role;
  const path = useHistory();
  if (isAuth && role === 'travelAdmin') {
    return (
      <Fragment>
        <Switch>
          <Route exact path={`/${urlHelper.travelAdmin.index}`} component={Requests} />
        </Switch>
      </Fragment>
    );
  }
  // REDIRECT TO LOGIN
  path.replace('/');
};

export default travelAdminRoutes;
