import Layout from 'antd/lib/layout/layout';
import React, { Fragment } from 'react';
import { Switch, Route, useHistory } from 'react-router-dom';
import Places from '../../views/Places';
import * as urlHelper from '../../helpers/urlHelper';
import { userData } from '../../helpers/currentUser';

const RequestsStatus = React.lazy(() => import('../../views/RequestsStatus'));
// const Places = React.lazy(() => import('../../views/Places'))
const PlaceDetails = React.lazy(() => import('../../components/PlaceDetails'));

const { Content } = Layout;

// eslint-disable-next-line consistent-return
const travellerRoutes = () => {
  const isAuth = userData()?.isAuth;
  const role = userData()?.user?.user.role;
  const path = useHistory();

  if (isAuth && role === 'traveller') {
    return (
      <Fragment>
        <Layout>
          <Content>
            <Switch>
              <Route exact path={`/${urlHelper.traveller.index}`} component={Places} />
              <Route exact path={`/${urlHelper.traveller.requests}`} component={RequestsStatus} />
              <Route path={`/${urlHelper.traveller.placeDetails}`} component={PlaceDetails}></Route>
            </Switch>
          </Content>
        </Layout>
      </Fragment>
    );
  }

  // REDIRECT TO LOGIN
  path.replace('/');
};

export default travellerRoutes;
