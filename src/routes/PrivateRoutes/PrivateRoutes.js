import Layout from 'antd/lib/layout/layout';
import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import Navbar from '../../components/Navbar';
import * as urlHelper from '../../helpers/urlHelper';

const { Content } = Layout;

const Admin = React.lazy(() => import('../adminRoutes'));
const TravelAdmin = React.lazy(() => import('../travelAdminRoutes'));
const Traveller = React.lazy(() => import('../travellerRoutes'));
const RequestsStatus = React.lazy(() => import('../../views/RequestsStatus'));

const PrivateRoutes = () => (
  <Fragment>
    <Layout>
      <Navbar />
      <Content>
        <Switch>
          <Route path={`/${urlHelper.superAdmin.index}`} component={Admin} />
          <Route path={`/${urlHelper.travelAdmin.index}`} exact component={TravelAdmin} />
          <Route path={`/${urlHelper.traveller.index}`} component={Traveller} />
          <Route path={`/${urlHelper.traveller.requests}`} exact component={RequestsStatus} />
        </Switch>
      </Content>
    </Layout>
  </Fragment>
);

export default PrivateRoutes;
