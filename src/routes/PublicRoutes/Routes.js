import React, { Fragment, Suspense } from 'react';
import { Switch, Route } from 'react-router-dom';
import Spin from 'antd/lib/spin';

const Login = React.lazy(() => import('../../views/Login'));
const Register = React.lazy(() => import('../../views/Register'));
const ForgotPassword = React.lazy(() => import('../../views/ForgotPassword'));
const ResetPassword = React.lazy(() => import('../../views/ResetPassword'));
const PrivateRoutes = React.lazy(() => import('../PrivateRoutes'));

const Routes = () => (
  <Fragment>
    <Suspense fallback={<Spin />}>
      <Switch>
        <Route path="/" exact component={Login}></Route>
        <Route path="/signup" exact component={Register}></Route>
        <Route path="/forgotpwd" exact component={ForgotPassword}></Route>
        <Route path="/resetpwd" exact component={ResetPassword}></Route>
        <Route component={PrivateRoutes}></Route>
      </Switch>
    </Suspense>
  </Fragment>
);

export default Routes;
