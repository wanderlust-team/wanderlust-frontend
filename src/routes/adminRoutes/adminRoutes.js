import React, { Fragment } from 'react';
import { Switch, Route, useHistory } from 'react-router-dom';
import SuperAdminNavbar from '../../components/SuperAdminNavbar';
import * as urlHelper from '../../helpers/urlHelper';
import { userData } from '../../helpers/currentUser';

const Admins = React.lazy(() => import('../../views/Admins'));
const Users = React.lazy(() => import('../../views/Users'));

// eslint-disable-next-line consistent-return
const adminRoutes = () => {
  const isAuth = userData()?.isAuth;
  const role = userData()?.user?.user.role;
  const path = useHistory();

  if (isAuth && role === 'superAdmin') {
    return (
      <Fragment>
        <SuperAdminNavbar />
        <Switch>
          <Route exact path={`/${urlHelper.superAdmin.index}`} component={Admins} />
          <Route exact path={`/${urlHelper.superAdmin.users}`} component={Users} />
        </Switch>
      </Fragment>
    );
  }

  // REDIRECT TO LOGIN
  path.replace('/');
};

export default adminRoutes;
