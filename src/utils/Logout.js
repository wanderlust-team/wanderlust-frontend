import { CURRENT_USER } from './constants';

function Logout() {
  localStorage.removeItem(CURRENT_USER);
}
export default Logout;
