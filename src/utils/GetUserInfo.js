// eslint-disable-next-line camelcase
import jwt_decode from 'jwt-decode';

export function GetUserInfo() {
  const token = localStorage.getItem('wonderLustToken');
  const userData = jwt_decode(token);
  return (userData.user);
}
