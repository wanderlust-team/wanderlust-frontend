/* eslint-disable react/prop-types */
import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import Typography from 'antd/lib/typography';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import UsersTable from '../../components/UsersTable/UsersTable';
import getUsers from '../../redux/actions/users/getUsers';

const { Title } = Typography;

const Users = ({ getUsers, users }) => {
  useEffect(() => {
    getUsers();
  }, []);

  return (
    <Fragment>
      <Row style={{ padding: '10px' }}>
        <Col xs={{ span: 24, offset: 0 }} sm={{ span: 16, offset: 4 }}>
          <Title level={3} style={{ padding: '15px 0' }}>
            Users ({users.allUsers.length})
          </Title>

          <UsersTable isLoading={users.getUsers.loading} allUsers={users.allUsers} />
        </Col>
      </Row>
    </Fragment>
  );
};

const mapState = (state) => ({ users: state.users });
export default connect(mapState, { getUsers })(Users);
