/* eslint-disable react/prop-types */
import { connect } from 'react-redux';
import React, { useEffect } from 'react';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import Typography from 'antd/lib/typography';
import AdminsTable from '../../components/AdminsTable';

import getAdmins from '../../redux/actions/admin/getAdmins';

const { Title } = Typography;

function Admins({ admins, getAdmins }) {
  useEffect(() => {
    getAdmins();
  }, []);

  return (
    <Row style={{ padding: '10px' }}>
      <Col xs={{ span: 24, offset: 0 }} sm={{ span: 16, offset: 4 }}>
        <Title level={3} style={{ padding: '15px 0' }}>
          Trip admins ({admins.length})
        </Title>
        <AdminsTable admins={admins.allAdmins} isLoading={admins.getAdmins.loading} />
      </Col>
    </Row>
  );
}

const mapState = (state) => ({ admins: state.admins });
export default connect(mapState, { getAdmins })(Admins);
