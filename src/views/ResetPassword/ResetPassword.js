import React from 'react';
import * as Yup from 'yup';
import Input from 'antd/lib/input';
import EyeInvisibleOutlined from '@ant-design/icons/EyeInvisibleOutlined';
import EyeTwoTone from '@ant-design/icons/EyeTwoTone';
import { useFormik } from 'formik';
import Logo from '../../assets/img/Group 5.png';
import './index.css';

function ResetPassword() {
  const formik = useFormik({
    initialValues: {
      index: '343',
      password: '',
      confirm_password: ''
    },
    validationSchema: Yup.object({
      password: Yup.string().required('Password required'),
      confirm_password: Yup.string().required('Password required')
    }),
    onSubmit: () => {
      // alert(JSON.stringify(values));
    }
  });

  return (
    <div className="public_bg">
      <div className="loginform">
        <div className="signup_logo">
          <img src={Logo} alt="Logo" />
        </div>

        <p className="title_1 text_center signup_label">Set new password</p>
        <p className="title_6 text_center text_label">
          Make sure you'll set a password you'll remember
        </p>
        <br />

        <form onSubmit={formik.handleSubmit}>
          <Input.Password
            {...formik.getFieldProps('email')}
            type="password"
            name="password"
            placeholder="Enter password"
            className="input"
            iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
          />
          {formik.touched.password && formik.errors.password ? (
            <span className="error_msg">{formik.errors.password}</span>
          ) : null}

          <Input.Password
            {...formik.getFieldProps('email')}
            type="password"
            name="confirm_password"
            placeholder="Confrim password"
            className="input"
            iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
          />
          {formik.touched.confirm_password && formik.errors.confirm_password ? (
            <span className="error_msg">{formik.errors.confirm_password}</span>
          ) : null}

          <button type="submit" className="btn_primary bg_orange">
            SET NEW PASSWORD
          </button>
        </form>
      </div>
    </div>
  );
}

export default ResetPassword;
