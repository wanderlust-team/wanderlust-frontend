import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Spin from 'antd/lib/spin';
import Col from 'antd/lib/col';
import Row from 'antd/lib/row';
import Typography from 'antd/lib/typography';
import { connect } from 'react-redux';
import SingleRequest from '../../components/SingleRequest';
import getRequests from '../../redux/actions/requests/getRequests';
import './index.css';

const { Title } = Typography;

function Requests({ requests, getRequests }) {
  useEffect(() => {
    getRequests();
  }, []);

  return (
    <Row>
      <Col sm={{ span: 16, offset: 4 }} xs={{ span: 24, offset: 0 }}>
        <Title className="requests_title" level={3} style={{ padding: '15px 0' }}>
          Trip requests
        </Title>

        {requests.getRequests.loading ? (
          <Spin className="requests_spin" />
        ) : (
          requests.allRequests.map((request) => (
            <SingleRequest
              name={request.place.title}
              price={request.place.price}
              user_name={`${request.requestor.firstName} ${request.requestor.lastName}`}
              user_email={request.requestor.email}
              picture={request.place.picture}
              id={request.id}
              key={request.id}
              status={request.status}
            />
          ))
        )}
      </Col>
    </Row>
  );
}

Requests.propTypes = {
  requests: PropTypes.object,
  getRequests: PropTypes.func
};

const mapState = (state) => ({ requests: state.requests });
export default connect(mapState, { getRequests })(Requests);
