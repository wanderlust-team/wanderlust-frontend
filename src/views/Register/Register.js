/* eslint-disable quotes */
import React from 'react';
import PropTypes from 'prop-types';
import Button from 'antd/lib/button';
import Input from 'antd/lib/input';
import EyeInvisibleOutlined from '@ant-design/icons/EyeInvisibleOutlined';
import EyeTwoTone from '@ant-design/icons/EyeTwoTone';
import './index.css';
import { Link, useHistory } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import Logo from '../../assets/img/Group 5.png';
import registerAction from '../../redux/actions/register/register';

function Register({ register, registerAction }) {
  const path = useHistory();

  // Redirect when registered
  if (register?.register?.message) path.replace('/');

  const formik = useFormik({
    initialValues: {
      email: '',
      firstName: '',
      lastName: '',
      password: '',
      confirmpassword: ''
    },
    validationSchema: Yup.object({
      firstName: Yup.string().required('First name required'),
      lastName: Yup.string().required('Last name required'),
      email: Yup.string().email().required('Email required'),
      password: Yup.string().required('Password required'),
      confirmpassword: Yup.string()
        .oneOf([Yup.ref('password'), null], "Passwords don't match!")
        .required('Password required')
    }),
    onSubmit: ({ email, firstName, lastName, password }) => {
      registerAction({ email, firstName, lastName, password });
    }
  });

  return (
    <div className="public_bg">
      <div className="loginform">
        <div className="signup_logo">
          <img src={Logo} alt="Logo" />
        </div>

        <p className="title_1 text_center signup_label">Get on board</p>
        <p className="title_6 text_center text_label">Travel around the world with us.</p>
        <br />

        <form onSubmit={formik.handleSubmit}>
          <Input
            {...formik.getFieldProps('firstName')}
            type="text"
            name="firstName"
            className="input"
            placeholder="First name"
          />
          {formik.touched.firstName && formik.errors.firstName ? (
            <span className="error_msg">{formik.errors.firstName}</span>
          ) : null}

          <Input
            {...formik.getFieldProps('lastName')}
            type="text"
            name="lastName"
            className="input"
            placeholder="Last name"
          />
          {formik.touched.lastName && formik.errors.lastName ? (
            <span className="error_msg">{formik.errors.lastName}</span>
          ) : null}

          <Input
            {...formik.getFieldProps('email')}
            type="email"
            name="email"
            className="input"
            placeholder="Email"
          />
          {formik.touched.email && formik.errors.email ? (
            <span className="error_msg">{formik.errors.email}</span>
          ) : null}

          <Input.Password
            {...formik.getFieldProps('password')}
            name="password"
            className="input"
            placeholder="Password"
            iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
          />
          {formik.touched.password && formik.errors.password ? (
            <span className="error_msg">{formik.errors.password}</span>
          ) : null}

          <Input.Password
            {...formik.getFieldProps('confirmpassword')}
            name="confirmpassword"
            className="input"
            placeholder="Confirm password"
            iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
          />
          {formik.touched.confirmpassword && formik.errors.confirmpassword ? (
            <span className="error_msg">{formik.errors.confirmpassword}</span>
          ) : null}
          <Button
            htmlType="submit"
            loading={register.register.loading}
            className="btn_primary bg_orange"
          >
            Sign up
          </Button>
        </form>

        <Link to="/">
          <p className="title_5 text_center">Login</p>
        </Link>
      </div>
    </div>
  );
}

// Register.PropTypes = { register: PropTypes.string };
Register.propTypes = { register: PropTypes.object, registerAction: PropTypes.func };

const mapState = (state) => ({ register: state.register });
export default connect(mapState, { registerAction })(Register);
