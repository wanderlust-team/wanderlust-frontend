import Layout from 'antd/lib/layout';
import Breadcrumb from 'antd/lib/breadcrumb';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import React from 'react';
import { Link } from 'react-router-dom';
import SingleCard from '../../components/SingleCard';
import Picture from '../../assets/img/gollira.jpg';
import Picture2 from '../../assets/img/kibuye.jpg';
import Picture3 from '../../assets/img/programmer.jpg';
import './index.css';

const { Content } = Layout;

const RequestsStatus = () => (
  <Layout>
    <Content style={{ padding: '10px' }}>
      <Row>
        <Col span={16} offset={4}>
          <Breadcrumb style={{ margin: '16px 20px' }}>
            <Breadcrumb.Item>
              <Link to="/places">Explore</Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>Requests</Breadcrumb.Item>
          </Breadcrumb>

          <p className="title_1" style={{ marginLeft: '20px' }}>
            {requests.length} Trip Requests
          </p>
          <br />

          <div className="all_cards">
            {requests.map((request) => (
              <SingleCard
                key={request.id}
                status={request.status}
                name={request.title}
                price={request.price}
                id={request.id}
                picture={request.picture}
              />
            ))}
          </div>
        </Col>
      </Row>
    </Content>
  </Layout>
);

export default RequestsStatus;

const requests = [
  {
    title: 'Missisip',
    email: 'jeanmarieissa@gmail.com',
    price: '40000',
    requester: 'Issa Jean Marie',
    picture: Picture,
    status: 'REJECTED',
    id: 34
  },
  {
    title: 'Nyungwe',
    email: 'glayds@gmail.com',
    price: '40,000',
    requester: 'Gladys INABEZA',
    picture: Picture3,
    status: 'PENDING',
    id: 1
  },
  {
    title: 'Montana',
    email: 'kariza@gmail.com',
    price: '45,000',
    requester: 'Karigenzi Franscine',
    picture: Picture2,
    status: 'APPROVED',
    id: 3
  }
];
