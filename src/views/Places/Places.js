import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import Col from 'antd/lib/col';
import Row from 'antd/lib/row';
import Spin from 'antd/lib/spin';
import { connect } from 'react-redux';
import Typography from 'antd/lib/typography';
import SingleCard from '../../components/SingleCard';
import getPlaces from '../../redux/actions/places/getPlaces';
import './index.css';

const { Title } = Typography;

function Places({ getPlaces, places }) {
  useEffect(() => {
    getPlaces();
  }, []);

  const showLoader = <Spin className="empty-spin" />;
  return (
    <Fragment>
      <Row style={{ padding: '10px' }} wrap={true}>
        <Col lg={{ span: 16, offset: 4 }} sm={{ span: 24, offset: 0 }} md={{ span: 20, offset: 2 }}>
          <Title className="places_title" level={3} style={{ padding: '15px 0' }}>
            Explore ({places.allPlaces.length})
          </Title>

          <Row>
            {places.getPlaces.loading
              ? showLoader
              : places.allPlaces.map((place) => (
                  <Col
                    key={place.id}
                    lg={8}
                    md={12}
                    sm={24}
                    style={{ marginBottom: '20px', padding: '0 10px' }}
                  >
                    <SingleCard
                      name={place.title}
                      price={place.price}
                      picture={place.picture}
                      id={place.id}
                    />
                  </Col>
                  // eslint-disable-next-line indent
                ))}
          </Row>
        </Col>
      </Row>
    </Fragment>
  );
}

Places.propTypes = {
  getPlaces: PropTypes.func,
  places: PropTypes.object
};

const mapState = (state) => ({ places: state.places });
export default connect(mapState, { getPlaces })(Places);
