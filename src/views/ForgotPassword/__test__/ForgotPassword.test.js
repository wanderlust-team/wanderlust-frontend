import React from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
import ForgotPassword  from '../ForgotPassword'
import { act } from 'react-dom/test-utils'
import { BrowserRouter } from 'react-router-dom'

let container = null;
beforeEach(() => {
    //Prepare the rendering area
    container = document.createElement('div')
    document.body.appendChild(container)
})

afterEach(() => {
    //Clean up area after each test
    unmountComponentAtNode(container)
    container.remove()
    container = null
})

describe("Forgot password", () => {
    it("Renders  without crashing", () => {
        act(() => {
            render(<BrowserRouter><ForgotPassword /></BrowserRouter>, container)
        })
        expect(container).toBeTruthy();        
    })
    
    it("Has the text", () => {
        act(() => {
            render(<BrowserRouter><ForgotPassword /></BrowserRouter>, container)
        })
        expect(container.textContent).toContain('Signup')
    })
})