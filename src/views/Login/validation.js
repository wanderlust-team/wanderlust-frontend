import * as Yup from 'yup';
import { email, password } from '../../helpers/inputValidation';

export const initialValues = {
  email: '',
  password: ''
};

export const validationSchema = Yup.object().shape({
  email,
  password
});

// validationSchema: Yup.object({
//     email: Yup.string().email().required('Required'),
//     password: Yup.string().required('Required')
//   })
