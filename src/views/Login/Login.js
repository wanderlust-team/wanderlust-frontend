/* eslint-disable react/prop-types */
import React, { Fragment } from 'react';
import Button from 'antd/lib/button';
import { Link, useHistory } from 'react-router-dom';
import './index.css';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import Form from 'formik-antd/lib/form';
import Input from 'formik-antd/lib/input';
import { userData } from '../../helpers/currentUser';
import useHandleSideEffect from '../../hooks/useHandleSideEffect';
import loginAction from '../../redux/actions/login/login';
import Logo from '../../assets/img/logo.png';
import { initialValues, validationSchema } from './validation';
import * as urlHelper from '../../helpers/urlHelper';

export const Login = ({ loginAction, login }) => {
  const history = useHistory();

  const redirectByRole = () => {
    if (userData()?.isAuth) {
      if (userData()?.user?.user?.role === 'superAdmin') {
        return history.push(urlHelper.superAdmin.index);
      }
      if (userData()?.user?.user?.role === 'traveller') {
        return history.push(urlHelper.traveller.index);
      }
      if (userData()?.user?.user?.role === 'travelAdmin') {
        return history.push(urlHelper.travelAdmin.index);
      }
    }

    return null;
  };

  const handleError = () => {
    // console.log('HANDLE ERROR: ', error);
  };

  const handleRender = () => {
    redirectByRole();
  };

  const redirectUser = () => {
    redirectByRole();
  };

  useHandleSideEffect({
    onFailure: handleError,
    onSuccess: redirectUser,
    onRender: handleRender,
    onLoading: () => {
      // console.log('HANDLE LOADING...');
    },
    state: login.login
  });

  return (
    <Fragment>
      <div className="public_bg">
        <div className="loginform">
          <div className="login_logo">
            <img src={Logo} alt="Logo" />
          </div>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={({ email, password }) => {
              loginAction({ email, password });
            }}
          >
            {({ errors, touched }) => (
              <Form>
                <Form.Item
                  style={{ marginBottom: '0' }}
                  name="email"
                  validateStatus={errors.email && touched.email ? 'error' : ''}
                >
                  <Input placeholder="Email" name="email" className="input" type="email" />
                </Form.Item>
                <Form.Item
                  style={{ marginBottom: '0' }}
                  name="password"
                  validateStatus={errors.password && touched.password ? 'error' : ''}
                >
                  <Input.Password
                    placeholder="Password"
                    name="password"
                    className="input"
                    type="password"
                  />
                </Form.Item>

                <Button
                  loading={login.login.loading}
                  htmlType="submit"
                  className="btn_primary bg_orange"
                >
                  Login
                </Button>
              </Form>
            )}
          </Formik>

          <Link to="/signup">
            <p className="title_6 text_center text_white_green">Or create new account</p>
          </Link>

          <Link to="/forgotpwd">
            <p className="title_5 text_center">Forgot password?</p>
          </Link>
        </div>
      </div>
    </Fragment>
  );
};

const mapState = (state) => ({ login: state.login });
export default connect(mapState, { loginAction })(Login);
