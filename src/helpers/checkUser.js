export default () => {
  try {
    const isAuth = !!localStorage.CURRENT_USER;
    return isAuth;
  } catch (error) {
    return false;
  }
};
