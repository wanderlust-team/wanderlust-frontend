/* eslint-disable operator-linebreak */
import axios from 'axios';
import * as urlHelper from './urlHelper';
import { userToken } from './currentUser';

const { baseUrl } = urlHelper.backend;

export const publicFetch = axios.create({ baseURL: process.env.REACT_WANDERLUST_BASE_URL_BACKEND });

export default (data = {}) => {
  const WANDERLUST_TOKEN = userToken() || null;

  const { token, responseType } = data;
  const baseURL = baseUrl;
  const jwt = token || WANDERLUST_TOKEN || undefined;
  let headers = jwt ? { Authorization: `Bearer ${jwt}` } : '';
  if (data.headers) {
    headers = { ...headers, 'content-type': data.headers['Content-Type'] };
  }
  return axios.create({ baseURL, headers, responseType });
};
