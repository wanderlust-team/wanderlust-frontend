import { actionTypes } from '../redux/actionTypes';

export default ({
  url = '',
  method = 'GET',
  data = null,
  onStart = actionTypes.api.API_REQUEST_START,
  onEnd = actionTypes.api.API_REQUEST_END,
  onSuccess = actionTypes.api.API_REQUEST_SUCCESS,
  onFailure = actionTypes.api.API_REQUEST_FAILURE
}) => ({
  type: actionTypes.api.API_REQUEST,
  payload: {
    url,
    method,
    data,
    onStart,
    onEnd,
    onSuccess,
    onFailure
  }
});
