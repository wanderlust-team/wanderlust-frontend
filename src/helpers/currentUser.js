import JwtDecode from 'jwt-decode';
import { CURRENT_USER } from '../utils/constants';

export const userToken = () => JSON.parse(localStorage.getItem(CURRENT_USER))?.user?.accessToken;

export const userData = () => {
  const userInfo = JSON.parse(localStorage.getItem(CURRENT_USER));
  if (userInfo) {
    return {
      isAuth: userInfo.isAuth,
      user: JwtDecode(userInfo.user.accessToken)
    };
  }
  return userInfo;
};
