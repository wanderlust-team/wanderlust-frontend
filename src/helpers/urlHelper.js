/* eslint-disable object-curly-newline */
const { REACT_WANDERLUST_BASE_URL_BACKEND } = process.env;

const backend = {
  baseUrl: REACT_WANDERLUST_BASE_URL_BACKEND || 'https://wanderlust-ij.herokuapp.com/api/'
};

const superAdmin = {
  index: 'super-admin',
  users: 'super-admin/users'
};

const traveller = {
  index: 'traveller',
  places: 'traveller/places',
  placeDetails: 'traveller/places/:id',
  requests: 'traveller/requests'
};

const travelAdmin = {
  index: 'travel-admin',
  requests: 'travel-admin/requests'
};

export { backend, superAdmin, travelAdmin, traveller };
