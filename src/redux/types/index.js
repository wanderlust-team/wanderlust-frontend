export const USER_LOGIN = "USER_LOGIN";
export const USER_REGISTER = 'USER_REGISTER'
export const GET_PLACES = 'GET_PLACES'
export const GET_USERS = 'GET_USERS'
export const GET_ADMINS = 'GET_ADMINS'
export const GET_REQUESTS = 'GET_REQUESTS'
export const CHANGE_ROLE = 'CHANGE_ROLE'
export const GET_REQUESTS_STATUS = 'GET_REQUESTS_STATUS'
export const DELETE_USER = 'DELETE_USER'
export const GET_PLACE_DETAILS = 'GET_PLACE_DETAILS'
export const REQUEST_TRIP = 'REQUEST_TRIP'
export const ACCEPT_REQUEST = 'ACCEPT_REQUEST'
export const DELETE_REQUEST = 'DELETE_REQUEST'