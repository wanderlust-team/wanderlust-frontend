import apiAction from '../../../helpers/apiAction';
import { actionTypes } from '../../actionTypes';

export default (payload = {}) => (dispatch) =>
  dispatch(
    apiAction({
      method: 'post',
      url: 'auth/login',
      data: { ...payload },
      onStart: actionTypes.login.LOGIN_START,
      onEnd: actionTypes.login.LOGIN_END,
      onSuccess: actionTypes.login.LOGIN_SUCCESS,
      onFailure: actionTypes.login.LOGIN_FAILLURE
    })
  );
