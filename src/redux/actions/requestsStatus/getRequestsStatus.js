import apiAction from '../../../helpers/apiAction';
import { actionTypes } from "../../actionTypes";

export default () => (dispatch) => 
    dispatch(
        apiAction({
            method: 'get',
            url: 'requests/traveller',
            onStart: actionTypes.requests_status.GET_REQUESTS_STATUS_START,
            onEnd: actionTypes.requests_status.GET_REQUESTS_STATUS_END,
            onSuccess: actionTypes.requests_status.GET_REQUESTS_STATUS_SUCCESS,
            onFailure: actionTypes.requests_status.GET_REQUESTS_STATUS_FAILURE
        })
    )