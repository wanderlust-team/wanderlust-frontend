import apiAction from '../../../helpers/apiAction';
import { actionTypes } from '../../actionTypes';

export default () => (dispatch) =>
  dispatch(
    apiAction({
      method: 'get',
      url: 'places',
      onStart: actionTypes.places.GET_PLACES_START,
      onEnd: actionTypes.places.GET_PLACES_END,
      onSuccess: actionTypes.places.GET_PLACES_SUCCESS,
      onFailure: actionTypes.places.GET_PLACES_FAILURE
    })
  );
