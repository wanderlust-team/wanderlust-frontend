import { apiAction } from '../../../helpers/apiAction';
import { actionTypes } from "../../actionTypes";

export default (id) => (dispatch) => 
    dispatch(
        apiAction({
            method: 'get',
            url: `places/${id}`,
            onStart: actionTypes.places.GET_PLACE_DETAILS_START,
            onEnd: actionTypes.places.GET_PLACE_DETAILS_END,
            onSuccess: actionTypes.places.GET_PLACE_DETAILS_SUCCESS,
            onFailure: actionTypes.places.GET_PLACE_DETAILS_FAILURE
        })
    )