import apiAction from '../../../helpers/apiAction';
import { actionTypes } from '../../actionTypes';

export default (email, payload = {}) => (dispatch) =>
  dispatch(
    apiAction({
      method: 'patch',
      url: 'admin/users/' + email,
      data: { ...payload },
      onStart: actionTypes.users.UPDATE_USER_START,
      onEnd: actionTypes.users.UPDATE_USER_END,
      onSuccess: actionTypes.users.UPDATE_USER_SUCCESS,
      onFailure: actionTypes.users.UPDATE_USER_FAILURE
    })
  );
