import apiAction from '../../../helpers/apiAction';
import { actionTypes } from '../../actionTypes';

export default () => (dispatch) =>
  dispatch(
    apiAction({
      method: 'get',
      url: 'admin/users?type=users',
      onStart: actionTypes.users.GET_USERS_START,
      onEnd: actionTypes.users.GET_USERS_END,
      onSuccess: actionTypes.users.GET_USERS_SUCCESS,
      onFailure: actionTypes.users.GET_USERS_FAILURE
    })
  );
