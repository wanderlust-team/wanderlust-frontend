import apiAction from '../../../helpers/apiAction';
import { actionTypes } from '../../actionTypes';

export default (payload = {}) => (dispatch) => {
  return dispatch(
    apiAction({
      method: 'post',
      url: 'auth/signup',
      data: { ...payload },
      onStart: actionTypes.register.REGISTER_START,
      onEnd: actionTypes.register.REGISTER_END,
      onSuccess: actionTypes.register.REGISTER_SUCCESS,
      onFailure: actionTypes.register.REGISTER_FAILURE
    })
  );
};
