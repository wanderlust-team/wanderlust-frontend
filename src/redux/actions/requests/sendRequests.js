import apiAction from "../../../helpers/apiAction";
import { actionTypes } from "../../actionTypes";

export default (id, payload = {}) => (dispatch) =>
    dispatch(
        apiAction({
            method: 'post',
            url: `requests/${id}`,
            data: { ...payload },
            onStart: actionTypes.requests.SEND_REQUEST_START,
            onEnd: actionTypes.requests.SEND_REQUEST_END,
            onSuccess: actionTypes.requests.SEND_REQUEST_SUCCESS,
            onFailure: actionTypes.requests.SEND_REQUEST_FAILURE
        })
    );
