import apiAction from "../../../helpers/apiAction";
import { actionTypes } from "../../actionTypes";

export default (id, payload = {}) => (dispatch) =>
    dispatch(
        apiAction({
            method: 'patch',
            url: `requests/approve/${id}`,
            data: { ...payload },
            onStart: actionTypes.requests.ACCEPT_REQUESTS_START,
            onEnd: actionTypes.requests.ACCEPT_REQUESTS_END,
            onSuccess: actionTypes.requests.ACCEPT_REQUESTS_SUCCESS,
            onFailure: actionTypes.requests.ACCEPT_REQUESTS_FAILURE
        })
    )