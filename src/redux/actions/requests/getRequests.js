import apiAction from "../../../helpers/apiAction";
import { actionTypes } from "../../actionTypes";

export default () => (dispatch) =>
    dispatch(
        apiAction({
            method: 'get',
            url: 'requests/travelAdmin',
            onStart: actionTypes.requests.GET_REQUESTS_START,
            onEnd: actionTypes.requests.GET_REQUESTS_END,
            onSuccess: actionTypes.requests.GET_REQUESTS_SUCCESS,
            onFailure: actionTypes.requests.GET_REQUESTS_FAILURE
        })
    )

