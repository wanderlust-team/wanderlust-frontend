import apiAction from "../../../helpers/apiAction";
import { actionTypes } from "../../actionTypes";

export default (id, payload = {}) => (dispatch) =>
    dispatch(
        apiAction({
            method: 'patch',
            url: `requests/reject/${id}`,
            data: { ...payload },
            onStart: actionTypes.requests.REJECT_REQUESTS_START,
            onEnd: actionTypes.requests.REJECT_REQUESTS_END,
            onSuccess: actionTypes.requests.REJECT_REQUESTS_SUCCESS,
            onFailure: actionTypes.requests.REJECT_REQUESTS_FAILURE
        })
    );
