import apiAction from '../../../helpers/apiAction';
import { actionTypes } from '../../actionTypes';

export default (id, payload = {}) => (dispatch) =>
  dispatch(
    apiAction({
      method: 'delete',
      url: `admin/users/${id}`,
      data: { ...payload },
      onStart: actionTypes.admin.DELETE_ADMIN_START,
      onEnd: actionTypes.admin.DELETE_ADMIN_END,
      onSuccess: actionTypes.admin.DELETE_ADMIN_SUCCESS,
      onFailure: actionTypes.admin.DELETE_ADMIN_FAILURE
    })
  );
