import apiAction from '../../../helpers/apiAction'
import { actionTypes } from '../../actionTypes'

export default () => (dispatch) =>
    dispatch(
        apiAction({
            method: 'get',
            url: `admin/users?type=travelAdmin`,
            onStart: actionTypes.admin.GET_ADMINS_START,
            onEnd: actionTypes.admin.GET_ADMINS_END,
            onSuccess: actionTypes.admin.GET_ADMINS_SUCCESS,
            onFailure: actionTypes.admin.GET_ADMINS_FAILURE
        })
    )