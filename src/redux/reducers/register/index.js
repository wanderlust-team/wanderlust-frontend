import { initialState } from '../../store/initialState';
import registerReducer from './registerReducer';

export default (state = initialState.register, action) => {
  const register = registerReducer(state, action);
  return register || state;
};
