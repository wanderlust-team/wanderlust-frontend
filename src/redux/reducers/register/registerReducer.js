import { actionTypes } from '../../actionTypes';

export default (state, { type, payload }) => {
  switch (type) {
    case actionTypes.register.REGISTER_START:
      return {
        ...state,
        register: {
          ...state.register,
          message: null,
          loading: true,
          error: null,
          success: false
        }
      };
    case actionTypes.register.REGISTER_END:
      return {
        ...state,
        register: {
          ...state.register,
          message: null,
          loading: false,
          error: null,
          success: false
        }
      };
    case actionTypes.register.REGISTER_SUCCESS:
      return {
        ...state,
        register: {
          ...state.register,
          message: payload.data,
          loading: false,
          error: null,
          success: true
        }
      };
    case actionTypes.register.REGISTER_FAILURE:
      return {
        ...state,
        register: {
          ...state.register,
          message: payload.data,
          loading: false,
          error: payload.data,
          success: false
        }
      };
    default:
      return null;
  }
};
