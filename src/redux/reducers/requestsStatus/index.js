import { initialState } from '../../store/initialState';
import requestsStatusReducer from './requestsStatusReducer';

export default (state = initialState.requests, action) => {
  const requestsStatus = requestsStatusReducer(state, action);
  return requestsStatus || state;
};
