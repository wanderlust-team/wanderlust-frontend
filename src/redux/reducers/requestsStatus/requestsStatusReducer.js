import { actionTypes } from '../../actionTypes';

export default (state, { type, payload }) => {
  switch (type) {
    case actionTypes.requests_status.GET_REQUESTS_STATUS_START:
      return {
        ...state,
        getRequestsStatus: {
          ...state.getRequestsStatus,
          message: null,
          loading: true,
          error: null,
          success: false
        }
      };
    case actionTypes.requests_status.GET_REQUESTS_STATUS_END:
      return {
        ...state,
        getRequestsStatus: {
          ...state.getRequestsStatus,
          message: null,
          loading: false,
          error: null,
          success: false
        }
      };
    case actionTypes.requests_status.GET_REQUESTS_STATUS_SUCCESS:
      return {
        ...state,
        allRequestsStatus: payload.payload.data,
        getRequestsStatus: {
          ...state.getRequestsStatus,
          message: payload.mesage,
          loading: false,
          error: null,
          success: true
        }
      };
    case actionTypes.requests_status.GET_REQUESTS_STATUS_FAILURE:
      return {
        ...state,
        getRequestsStatus: {
          ...state.getRequestsStatus,
          message: payload.mesage,
          loading: false,
          error: payload.mesage,
          success: true
        }
      };
    default:
      return null;
  }
};
