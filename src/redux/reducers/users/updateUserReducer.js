import { actionTypes } from '../../actionTypes';

export default (state, { type, payload }) => {
  switch (type) {
    case actionTypes.users.UPDATE_USER_START:
      return {
        ...state,
        updateUser: {
          ...state.updateUser,
          message: null,
          loading: true,
          error: null,
          success: false
        }
      };
    case actionTypes.users.UPDATE_USER_END:
      return {
        ...state,
        updateUser: {
          ...state.updateUser,
          message: null,
          loading: false,
          error: null,
          success: false
        }
      };
    case actionTypes.users.UPDATE_USER_SUCCESS:
      return {
        ...state,
        allUsers: payload.data,
        updateUser: {
          ...state.updateUser,
          message: payload.message,
          loading: false,
          error: null,
          success: true
        }
      };
    case actionTypes.users.UPDATE_USER_FAILURE:
      return {
        ...state,
        updateUser: {
          ...state.updateUser,
          message: null,
          error: payload.message,
          success: false
        }
      };
    default:
      return null;
  }
};
