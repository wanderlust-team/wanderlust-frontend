import { actionTypes } from '../../actionTypes';

export default (state, { type, payload }) => {
  switch (type) {
    case actionTypes.users.GET_USERS_START:
      return {
        ...state,
        getUsers: {
          ...state.getUsers,
          message: null,
          loading: true,
          error: null,
          success: false
        }
      };

    case actionTypes.users.GET_USERS_END:
      return {
        ...state,
        getUsers: {
          ...state.getUsers,
          message: null,
          loading: false,
          error: null,
          success: false
        }
      };
    case actionTypes.users.GET_USERS_SUCCESS:
      return {
        ...state,
        allUsers: payload.data,
        getUsers: {
          ...state.getUsers,
          message: payload.data,
          loading: false,
          error: null,
          success: true
        }
      };
    case actionTypes.users.GET_USERS_FAILURE:
      return {
        ...state,
        getUsers: {
          ...state.getUsers,
          message: payload.data,
          loading: false,
          error: payload,
          success: false
        }
      };
    default:
      return null;
  }
};

// import { actionTypes } from '../../actionTypes';

// export default (state, { type, payload }) => {
//   switch (type) {
//     case actionTypes.users.GET_USERS_START:
//       return {
//         ...state,
//         getUsers: {
//           ...state.getUsers,
//           message: null,
//           loading: true,
//           error: null,
//           success: false
//         }
//       };
//     case actionTypes.users.GET_USERS_END:
//       return {
//         ...state,
//         getUsers: {
//           ...state.getUsers,
//           message: null,
//           loading: false,
//           error: null,
//           success: false
//         }
//       };
//     case actionTypes.users.GET_USERS_SUCCESS:
//       return {
//         ...state,
//         allUsers: payload.data,
//         getUsers: {
//           ...state.getUsers,
//           message: payload.message,
//           loading: false,
//           error: null,
//           success: true
//         }
//       };
//     case actionTypes.users.GET_USERS_FAILURE:
//       return {
//         ...state,
//         getUsers: {
//           ...state.getUsers,
//           message: payload.message,
//           error: payload.message,
//           success: false,
//           loading: false
//         }
//       };
//     default:
//       return null;
//   }
// };
