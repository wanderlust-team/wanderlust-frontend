import { initialState } from '../../store/initialState';
import updateUserReducer from './updateUserReducer';
import getUsersReducer from './getUsersReducer';

export default (state = initialState.users, action) => {
  const updateUser = updateUserReducer(state, action);
  const getUsers = getUsersReducer(state, action);
  return getUsers || updateUser || state;
};
