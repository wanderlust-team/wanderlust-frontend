import { actionTypes } from '../../actionTypes';

export default (state, { type, payload }) => {
  switch (type) {
    case actionTypes.requests.APPROVE_REQUESTS_START:
      return {
        ...state,
        approveRequest: {
          ...state.approveRequest,
          message: null,
          loading: true,
          error: null,
          success: false
        }
      };
    case actionTypes.requests.APPROVE_REQUESTS_END:
      return {
        ...state,
        approveRequest: {
          ...state.approveRequest,
          message: null,
          loading: false,
          error: null,
          success: false
        }
      };
    case actionTypes.requests.APPROVE_REQUESTS_SUCCESS:
      return {
        ...state,
        allRequests: payload.payload.data,
        approveRequest: {
          ...state.approveRequest,
          message: payload.mesage,
          loading: false,
          error: null,
          success: true
        }
      };
    case actionTypes.requests.APPROVE_REQUESTS_FAILURE:
      return {
        ...state,
        approveRequest: {
          ...state.approveRequest,
          message: payload.mesage,
          loading: false,
          error: payload.mesage,
          success: true
        }
      };
    default:
      return null;
  }
};
