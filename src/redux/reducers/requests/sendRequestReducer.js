import { actionTypes } from '../../actionTypes';

export default (state, { type, payload }) => {
  switch (type) {
    case actionTypes.requests.SEND_REQUEST_START:
      return {
        ...state,
        sendRequest: {
          ...state.sendRequest,
          message: null,
          loading: true,
          error: null,
          success: false
        }
      };
    case actionTypes.requests.SEND_REQUEST_END:
      return {
        ...state,
        sendRequest: {
          ...state.sendRequest,
          message: null,
          loading: false,
          error: null,
          success: false
        }
      };
    case actionTypes.requests.SEND_REQUEST_SUCCESS:
      return {
        ...state,
        allRequests: payload.payload.data,
        sendRequest: {
          ...state.sendRequest,
          message: payload.mesage,
          loading: false,
          error: null,
          success: true
        }
      };
    case actionTypes.requests.SEND_REQUEST_FAILURE:
      return {
        ...state,
        sendRequest: {
          ...state.sendRequest,
          message: payload.mesage,
          loading: false,
          error: payload.mesage,
          success: true
        }
      };
    default:
      return null;
  }
};
