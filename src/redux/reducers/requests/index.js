import { initialState } from '../../store/initialState';
import approveRequestReducer from './approveRequestReducer';
import getRequestsReducer from './getRequestsReducer';
import rejectRequestReducer from './rejectRequestReducer';
import sendRequestReducer from './sendRequestReducer';

export default (state = initialState.requests, action) => {
  const approveRequest = approveRequestReducer(state, action);
  const getRequests = getRequestsReducer(state, action);
  const rejectRequest = rejectRequestReducer(state, action);
  const sendRequest = sendRequestReducer(state, action);
  return approveRequest || getRequests || rejectRequest || sendRequest || state;
};
