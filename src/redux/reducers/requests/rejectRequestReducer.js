import { actionTypes } from '../../actionTypes';

export default (state, { type, payload }) => {
  switch (type) {
    case actionTypes.requests.REJECT_REQUESTS_START:
      return {
        ...state,
        rejectRequest: {
          ...state.rejectRequest,
          message: null,
          loading: true,
          error: null,
          success: false
        }
      };
    case actionTypes.requests.REJECT_REQUESTS_END:
      return {
        ...state,
        rejectRequest: {
          ...state.rejectRequest,
          message: null,
          loading: false,
          error: null,
          success: false
        }
      };
    case actionTypes.requests.REJECT_REQUESTS_SUCCESS:
      return {
        ...state,
        allRequests: payload.payload.data,
        rejectRequest: {
          ...state.rejectRequest,
          message: payload.mesage,
          loading: false,
          error: null,
          success: true
        }
      };
    case actionTypes.requests.REJECT_REQUESTS_FAILURE:
      return {
        ...state,
        rejectRequest: {
          ...state.rejectRequest,
          message: payload.mesage,
          loading: false,
          error: payload.mesage,
          success: true
        }
      };
    default:
      return null;
  }
};
