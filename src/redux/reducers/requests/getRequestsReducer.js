import { actionTypes } from '../../actionTypes';

export default (state, { type, payload }) => {
  switch (type) {
    case actionTypes.requests.GET_REQUESTS_START:
      return {
        ...state,
        getRequests: {
          ...state.getRequests,
          message: null,
          loading: true,
          error: null,
          success: false
        }
      };
    case actionTypes.requests.GET_REQUESTS_END:
      return {
        ...state,
        getRequests: {
          ...state.getRequests,
          message: null,
          loading: false,
          error: null,
          success: false
        }
      };
    case actionTypes.requests.GET_REQUESTS_SUCCESS:
      return {
        ...state,
        allRequests: payload.data,
        getRequests: {
          ...state.getRequests,
          message: payload.mesage,
          loading: false,
          error: null,
          success: true
        }
      };
    case actionTypes.requests.GET_REQUESTS_FAILURE:
      return {
        ...state,
        getRequests: {
          ...state.getRequests,
          message: payload.mesage,
          loading: false,
          error: payload.mesage,
          success: true
        }
      };
    default:
      return null;
  }
};
