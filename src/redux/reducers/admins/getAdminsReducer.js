import { actionTypes } from '../../actionTypes';

export default (state, { type, payload }) => {
  switch (type) {
    case actionTypes.admin.GET_ADMINS_START:
      return {
        ...state,
        getAdmins: {
          ...state.getAdmins,
          message: null,
          loading: true,
          error: null,
          success: false
        }
      };
    case actionTypes.admin.GET_ADMINS_END:
      return {
        ...state,
        getAdmins: {
          ...state.getAdmins,
          message: null,
          loading: false,
          error: null,
          success: false
        }
      };
    case actionTypes.admin.GET_ADMINS_SUCCESS:
      return {
        ...state,
        allAdmins: payload.data,
        getAdmins: {
          ...state.getAdmins,
          message: payload,
          loading: false,
          error: null,
          success: true
        }
      };
    case actionTypes.admin.GET_ADMINS_FAILURE:
      return {
        ...state,
        getAdmins: {
          ...state.getAdmins,
          message: payload,
          loading: false,
          error: payload,
          success: true
        }
      };
    default:
      return null;
  }
};
