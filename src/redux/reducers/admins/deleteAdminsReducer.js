import { actionTypes } from '../../actionTypes';

export default (state, { type, payload }) => {
  switch (type) {
    case actionTypes.admin.DELETE_ADMIN_START:
      return {
        ...state,
        deleteAdmin: {
          ...state.deleteAdmin,
          message: null,
          loading: true,
          error: null,
          success: false
        }
      };
    case actionTypes.admin.DELETE_ADMIN_END:
      return {
        ...state,
        deleteAdmin: {
          ...state.deleteAdmin,
          message: null,
          loading: false,
          error: null,
          success: false
        }
      };
    case actionTypes.admin.DELETE_ADMIN_SUCCESS:
      return {
        ...state,
        deleteAdmin: {
          ...state.deleteAdmin,
          message: payload,
          loading: false,
          error: null,
          success: true
        }
      };
    case actionTypes.admin.DELETE_ADMIN_FAILURE:
      return {
        ...state,
        deleteAdmin: {
          ...state.deleteAdmin,
          message: payload,
          loading: false,
          error: payload,
          success: true
        }
      };
    default:
      return null;
  }
};
