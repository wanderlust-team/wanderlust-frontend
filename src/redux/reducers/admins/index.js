import { initialState } from '../../store/initialState';
import deleteAdminsReducer from './deleteAdminsReducer';
import getAdminsReducer from './getAdminsReducer';

export default (state = initialState.admins, action) => {
  const deleteAdmins = deleteAdminsReducer(state, action);
  const getAdmins = getAdminsReducer(state, action);
  return deleteAdmins || getAdmins || state;
};
