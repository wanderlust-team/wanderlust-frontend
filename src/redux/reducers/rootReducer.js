import { combineReducers } from "redux";
import acceptRequestReducer from "./acceptRequestReducer";
import adminsReducer from "./adminsReducer";
import authReducer from "./authReducer";
import changeRoleReducer from "./changeRoleReducer";
import deleteReducer from "./deleteReducer";
import deleteRequestReducer from "./deleteRequestReducer";
import placeDetailsReducer from "./placeDetailsReducer";
import placesReducer from "./placesReducer";
import registerReducer from './registerReducer'
import requestsReducer from "./requestsReducer";
import requestStatusReducer from "./requestStatusReducer";
import requestTripReducer from "./requestTripReducer";
import usersReducer from "./usersReducer";

const rootReducer = combineReducers({
  auth: authReducer,
  register: registerReducer,
  places: placesReducer,
  users: usersReducer,
  admins: adminsReducer,
  requests: requestsReducer,
  role: changeRoleReducer,
  requestStatus: requestStatusReducer,
  deleteUser: deleteReducer,
  getPlaceDetails: placeDetailsReducer,
  requestTrip: requestTripReducer,
  acceptRequest : acceptRequestReducer,
  deleteRequest: deleteRequestReducer
});

export default rootReducer;
