import admins from './admins';
import login from './login';
import places from './places';
import register from './register';
import requests from './requests';
import requestsStatus from './requestsStatus';
import users from './users';

export default {
    admins,
    login,
    places,
    register,
    requests,
    requestsStatus,
    users
}
