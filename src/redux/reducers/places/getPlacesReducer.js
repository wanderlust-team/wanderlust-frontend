import { actionTypes } from '../../actionTypes';

export default (state, { type, payload }) => {
  switch (type) {
    case actionTypes.places.GET_PLACES_START:
      return {
        ...state,
        getPlaces: {
          ...state.getPlaces,
          message: null,
          loading: true,
          error: null,
          success: false
        }
      };

    case actionTypes.places.GET_PLACES_END:
      return {
        ...state,
        getPlaces: {
          ...state.getPlaces,
          message: null,
          loading: false,
          error: null,
          success: false
        }
      };
    case actionTypes.places.GET_PLACES_SUCCESS:
      return {
        ...state,
        allPlaces: payload.data,
        getPlaces: {
          ...state.getPlaces,
          message: payload.data,
          loading: false,
          error: null,
          success: true
        }
      };
    case actionTypes.places.GET_PLACES_FAILURE:
      return {
        ...state,
        getPlaces: {
          ...state.getPlaces,
          message: payload.data,
          loading: false,
          error: payload,
          success: false
        }
      };
    default:
      return null;
  }
};
