import { initialState } from '../../store/initialState';
import getPlacesReducer from './getPlacesReducer';

export default (state = initialState.places, action) => {
  const getPlaces = getPlacesReducer(state, action);
  return getPlaces || state;
};
