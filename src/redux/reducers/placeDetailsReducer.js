import {
    GET_PLACE_DETAILS
  } from "../types";
  
  const initialState = {
    isLoading: false,
    data: {}
  };
  
  const placeDetailsReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case GET_PLACE_DETAILS:
        return {
          ...state,
          isLoading: payload?.isLoading ? true : false,
          data: payload
        };
      default:
        return state;
    }
  };

  export default placeDetailsReducer
  