import {
    CHANGE_ROLE
  } from "../types";
  
  const initialState = {
    isLoading: false,
    data: {}
  };
  
  const changeRoleReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case CHANGE_ROLE:
        return {
          ...state,
          isLoading: payload?.isLoading ? true : false,
          data: payload
        };
      default:
        return state;
    }
  };

  export default changeRoleReducer
  