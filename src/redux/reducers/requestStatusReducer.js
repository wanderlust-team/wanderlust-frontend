import { GET_REQUESTS_STATUS } from "../types";

  const initialState = {
    isLoading: false,
    data: {}
  };
  
  const requestsStatusReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case GET_REQUESTS_STATUS:
        return {
          ...state,
          isLoading: payload?.isLoading ? true : false,
          data: payload
        };
      default:
        return state;
    }
  };

  export default requestsStatusReducer
  