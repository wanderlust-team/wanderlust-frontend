import {
  USER_LOGIN
} from "../types";

const initialState = {
  isLoading: false,
  data: []
};

const authReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_LOGIN:
      return {
        ...state,
        isLoading: payload?.isLoading ? true : false,
        data: payload
      };
    default:
      return state;
  }
};

export default authReducer