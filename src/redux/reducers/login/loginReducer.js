import { actionTypes } from '../../actionTypes';
import { CURRENT_USER } from '../../../utils/constants';

export default (state, { type, payload }) => {
  switch (type) {
    case actionTypes.login.LOGIN_START:
      return {
        ...state,
        login: {
          ...state.login,
          message: null,
          loading: true,
          error: null,
          success: false
        }
      };
    case actionTypes.login.LOGIN_END:
      return {
        ...state,
        login: {
          ...state.login,
          message: null,
          loading: false,
          error: null,
          success: false
        }
      };
    case actionTypes.login.LOGIN_SUCCESS:
      localStorage.setItem(CURRENT_USER, JSON.stringify({ isAuth: true, user: payload.data }));
      // localStorage.CURRENT_USER = JSON.stringify(payload.data);
      return {
        ...state,
        isAuth: true,
        loginRes: payload.data,
        accessToken: payload.data.accessToken,

        login: {
          ...state.login,
          message: payload.data,
          loading: false,
          error: null,
          success: true
        }
      };
    case actionTypes.login.LOGIN_FAILLURE:
      return {
        ...state,
        login: {
          ...state.login,
          message: payload.data,
          loading: false,
          error: payload.data,
          success: false
        }
      };
    default:
      return null;
  }
};
