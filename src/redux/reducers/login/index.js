import { initialState } from '../../store/initialState';
import loginReducer from './loginReducer';

export default (state = initialState.login, action) => {
  const login = loginReducer(state, action);
  return login || state;
};
