import {
    GET_PLACES
  } from "../types";
  
  const initialState = {
    isLoading: false,
    data: {}
  };
  
  const placesReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case GET_PLACES:
        return {
          ...state,
          isLoading: payload?.isLoading ? true : false,
          data: payload
        };
      default:
        return state;
    }
  };

  export default placesReducer
  