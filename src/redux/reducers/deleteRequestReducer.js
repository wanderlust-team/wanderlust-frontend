import {
    DELETE_REQUEST
  } from "../types";
  
  const initialState = {
    isLoading: false,
    data: {}
  };
  
  const deleteRequestReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case DELETE_REQUEST:
        return {
          ...state,
          isLoading: payload?.isLoading ? true : false,
          data: payload
        };
      default:
        return state;
    }
  };

  export default deleteRequestReducer
  