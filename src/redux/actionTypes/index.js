import { adminActionTypes as admin } from './adminActionTypes'
import { loginActionTypes as login } from './loginActionTypes'
import { placesActionTypes as places } from './placesActionTypes'
import { registerActionTypes as register } from './registerActionTypes'
import { requestsActionTypes as requests } from './requestsActionTypes'
import { requestsStatusActionTypes as requests_status } from './requestsStatusActionTypes'
import { usersActionTypes as users } from './usersActionTypes'
import { apiActionTypes as api } from './apiActionTypes'

export const actionTypes = {
    admin,
    login, 
    places,
    register,
    requests,
    requests_status,
    users,
    api
}