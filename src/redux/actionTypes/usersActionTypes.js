export const usersActionTypes = {
    //GET USERS
    GET_USERS_START: 'GET_USERS_START',
    GET_USERS_END: 'GET_USERS_END',
    GET_USERS_SUCCESS: 'GET_USERS_SUCCESS',
    GET_USERS_FAILURE: 'GET_USERS_FAILURE',
    //UPDATE USERS
    UPDATE_USER_START: 'UPDATE_USER_START',
    UPDATE_USER_END: 'UPDATE_USER_END',
    UPDATE_USER_SUCCESS: 'UPDATE_USER_SUCCESS',
    UPDATE_USER_FAILURE: 'UPDATE_USER_FAILURE'
}