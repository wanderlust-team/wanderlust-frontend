export const registerActionTypes = {
    REGISTER_START: 'REGISTER_START',
    REGISTER_END: 'REGISTER_END',
    REGISTER_SUCCESS: 'REGISTER_SUCCESS',
    REGISTER_FAILURE: 'REGISTER_FAILURE',
}