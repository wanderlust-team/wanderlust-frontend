import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import thunk from "redux-thunk";
import reducers from "../reducers";
import { initialState } from "./initialState";
import apiMiddleWare from '../../middlewares/apiMiddleware';

export const middlewares = [thunk, apiMiddleWare];

export default createStore(
    combineReducers({...reducers}),
    initialState,
    composeWithDevTools(applyMiddleware(...middlewares))
);
