export const requestsInitialState = {
    allRequests: [],
    getRequests: {
        message: null,
        loading: false,
        error: null,
        success: false
    },
    approveRequest: {
        message: null,
        loading: false,
        error: null,
        success: false
    },
    rejectRequest: {
        message: null,
        loading: false,
        error: null,
        success: false
    },
    sendRequest: {
        message: null,
        loading: false,
        error: null,
        success: false
    }
}