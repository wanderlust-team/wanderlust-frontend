export const usersInitialState = {
    allUsers: [],
    getUsers: {
        message: null,
        loading: false,
        error: null,
        success: false
    },
    updateUser: {
        message: null,
        loading: false,
        error: null,
        success: false
    }
};
