export const adminsInitialState = {
    allAdmins: [],
    getAdmins: {
        message: null,
        loading: false,
        error: null,
        success: false
    },
    deleteAdmin: {
        message: null,
        loading: false,
        error: null,
        success: false
    }
}