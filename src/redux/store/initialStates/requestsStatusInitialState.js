export const requestsStatusInitialState = {
    allRequestsStatus: [],
    getRequestsStatus: {
        message: null,
        loading: false,
        error: null,
        success: false
    }
}