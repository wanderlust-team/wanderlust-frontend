export const loginInitialState = {
  isAuth: false,
  loginRes: null,
  login: {
    message: null,
    loading: false,
    error: null,
    success: false
  }
};
