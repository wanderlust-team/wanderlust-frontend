export const registerInitialState = {
    register: {
        message: null,
        loading: false,
        error: null,
        success: false
    }
};
