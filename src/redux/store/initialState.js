import { usersInitialState as users } from './initialStates/usersInitialState';
import { placesInitialState as places } from './initialStates/placesInitialState';
import { adminsInitialState as admins } from './initialStates/adminsInitialState';
import { requestsInitialState as requests } from './initialStates/requestsInitialState';
import { requestsStatusInitialState as requestsStatus } from './initialStates/requestsStatusInitialState';
import { loginInitialState as login } from './initialStates/loginInitialState';
import { registerInitialState as register } from './initialStates/registerInitialState';

export const initialState = {
  users,
  places,
  admins,
  requests,
  requestsStatus,
  login,
  register
};
