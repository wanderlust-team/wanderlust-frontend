export default {
  login: { name: 'Login', url: 'auth/login' },
  signup: { name: 'Signup', url: 'auth/signup' }
};
