import { useEffect } from 'react';

const useHandleSideEffect = ({
  onSuccess = () => {},
  onFailure = () => {},
  onLoading = () => {},
  onRender = () => {},
  state = {
    error: null,
    loading: false,
    message: null,
    success: false
  }
}) => {
  useEffect(() => {
    if (state.success) {
      onSuccess();
    }
  }, [state.success]);

  useEffect(() => {
    if (state.error) {
      onFailure(state.error);
    }
  }, [state.error]);

  useEffect(() => {
    if (state.loading) {
      onLoading();
    }
  }, [state.loading]);

  useEffect(() => {
    onRender();
  }, []);
};

export default useHandleSideEffect;
